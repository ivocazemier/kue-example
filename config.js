const CONFIG = {
    QUEUE_NAME: 'myQueue',
    CONCURRENCY: 10,
    REDIS_URI: 'redis://localhost:6379',
    WEB_PORT: 1337,
    KUE_GUI_PORT: 3000,
    REMOVE_ON_COMPLETE: true
};


module.exports = CONFIG;
