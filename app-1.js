'use strict';

const path = require('path');
const express = require('express');
const expressApp = express();
const server = require('http').Server(expressApp);
const bodyParser = require('body-parser');
const kue = require('kue');
const io = require('socket.io')(server);
const CONFIG = require('./config');


/**
 * Connect with Redis
 * @type {Queue}
 */
const queue = kue.createQueue({
    redis: CONFIG.REDIS_URI
});

/**
 *
 * @param {Socket} socket
 * @param {*} data
 * @param {function} callback
 */
function enqueueJob(socket, data, callback) {

    const job = queue.create(CONFIG.QUEUE_NAME, {
        title: 'Queue some data',
        data: data
    });

    job.attempts(5);
    job.backoff({delay: 10 * 1000, type: 'fixed'});
    job.ttl(60 * 1000);
    job.removeOnComplete(CONFIG.REMOVE_ON_COMPLETE);


    job.on('complete', function (result) {
        socket.emit('complete', result);
    });

    job.on('failed attempt', function (errorMessage, doneAttempts) {
        console.log(errorMessage);
        console.log('attempts:' + doneAttempts);
    });

    job.on('failed', function (errorMessage) {
        socket.emit('error', errorMessage);
    });

    job.on('progress', function (progress, data) {
        socket.emit('progress', {progress: progress, job: job.id});
    });


    job.save(function (err) {
        callback(err);
    });
}


/**
 * Start listening on socket.io connection
 */
io.on('connection', function (socket) {

    socket.on('They took our jobs!', function (data) {
        enqueueJob(socket, data, (err) => {

            if (err) {
                socket.emit('error', err);
            } else {
                socket.emit('progress', {progress: 'your job is queued!'});
            }

        });
    });
});

/////////////////////////////////////////////////////////////////////////////
// parse application/x-www-form-urlencoded
expressApp.use(bodyParser.urlencoded({extended: false}));

// parse application/json
expressApp.use(bodyParser.json());

/**
 * Serve and route directly to socket.io client lib
 */
expressApp.use(express.static(path.join(__dirname, 'node_modules/socket.io-client/dist/')));

/**
 * Root route, serve index.html with ref to socket.io client above.
 */
expressApp.get('/', function (req, res, next) {
    res.sendFile(__dirname + '/index.html');
});

/**
 * Start listening on http port 1337
 * @type {number}
 */

server.listen(CONFIG.WEB_PORT, () => {
    console.log('Express server started on port %s', CONFIG.WEB_PORT);

    // Start kue's GUI
    kue.app.listen(CONFIG.KUE_GUI_PORT);
});


/////////////////
process.once('SIGTERM', function (sig) {
    queue.shutdown(5000, function (err) {
        console.log('Kue shutdown: ', err || '');
        process.exit(0);
    });
});
