
# How do I get set up? #

* `brew install parallel`
* `nvm install`
* `docker run -p 6379:6379 redis:3.0.7`

# For a chunked response from Express:
* `node app.js`
* Open another terminal: `node worker.js`
* Open another terminal: `cat url.list | parallel -j 10 curl`

# For a small socket.io example:
* `node app-1.js`
* Open another terminal: `node worker.js`
* Open several brower tabs and navigate to [http://localhost:1337](http://localhost:1337)
