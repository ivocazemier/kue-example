'use strict';
const kue = require('kue');
const EventThumper = require('event-thumper');
const CONFIG = require('./config');

const queue = kue.createQueue({
    redis: CONFIG.REDIS_URI
});
queue.watchStuckJobs(1000);

const eventThumper = EventThumper.getInstance();
eventThumper.startThumper();

/**
 *
 * @constructor
 */
function MyWorker() {

    this.counter = 0;
}

/**
 * Start a simulated long process
 * @param job
 * @param callback
 */
MyWorker.prototype.start = function (job, callback) {

    const self = this;

    eventThumper.on(eventThumper._200MS_, function processUnicorns() {

        self.counter++;

        if (self.counter % 10 === 0) {
            job.progress(self.counter, 100);
            job.log('Progress:' + self.counter + '%');
        }

        if (self.counter >= 100) {
            eventThumper.removeListener(eventThumper._200MS_, processUnicorns);
            callback(null, {result: 'success, job finished!'});
        }
    });
};

/**
 * Have a listener on the queue, which:
 * - Can handle 10 concurrent jobs
 * - Listens to the 'myJob' queue
 */
queue.process(CONFIG.QUEUE_NAME, CONFIG.CONCURRENCY, function (job, done) {

    const worker = new MyWorker();
    worker.start(job, (error, workerResult) => {

        done(error, workerResult)

    });

});

/**
 * Gracefull shutdown queue as documented
 */
process.once('SIGTERM', function (sig) {
    queue.shutdown(5000, function (err) {
        console.log('Kue shutdown: ', err || '');
        process.exit(0);
    });
});
