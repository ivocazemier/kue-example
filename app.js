'use strict';


const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const kue = require('kue');
const CONFIG = require('./config');


const expressApp = express();



const queue = kue.createQueue({
    redis: CONFIG.REDIS_URI
});


function enqueueJob(expressReq, expressRes, callback) {
    const data = expressReq.body;
    const res = expressRes;

    const job = queue.create(CONFIG.QUEUE_NAME, data);

    job.attempts(5);
    job.backoff({delay: 10 * 1000, type: 'fixed'});
    job.ttl(60 * 1000);
    job.removeOnComplete(CONFIG.REMOVE_ON_COMPLETE);


    job.on('complete', function (result) {
        res.status(200).end(JSON.stringify(result, null, 2));
    });

    job.on('failed attempt', function (errorMessage, doneAttempts) {
        console.log(errorMessage);
        console.log('attempts:' + doneAttempts);
    });

    job.on('failed', function (errorMessage) {
        console.log(errorMessage);
    });

    job.on('progress', function (progress, data) {
        console.info(`Progress: ${progress} for job:${job.id}`);
        res.status(200).write(JSON.stringify({result: progress}, null, 2));
    });


    job.save(function (err) {
        callback(err);
    });
}

/////////////////////////////////////////////////////////////////////////////////////
// parse application/x-www-form-urlencoded
expressApp.use(bodyParser.urlencoded({extended: false}));

// parse application/json
expressApp.use(bodyParser.json());

expressApp.use(express.static(__dirname + '/bower_components'));
expressApp.use(function (req, res) {

    enqueueJob(req, res, (err) => {
        if (err) {
            return res.status(500).end(JSON.stringify({error: err}));
        }
        // res.append('Connection', 'Transfer-Encoding');
        res.append('Content-Type', 'application/json; charset=utf-8');
        // res.append('Transfer-Encoding', 'chunked');
        res.write(JSON.stringify({result: 'your job is queued!'}, null, 2));

    });
});

const httpServer = http.createServer(expressApp);

httpServer.listen(CONFIG.WEB_PORT, () => {
    console.log('Express server started on port %s', CONFIG.WEB_PORT);
    kue.app.listen(CONFIG.KUE_GUI_PORT);
});


/////////////////
process.once('SIGTERM', function (sig) {
    queue.shutdown(5000, function (err) {
        console.log('Kue shutdown: ', err || '');
        process.exit(0);
    });
});
